<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([[
                    'first_name' => 'Alequei',
                    'last_name' => 'Solis',
                    'id_role' => 2
                ],
                [
                     'first_name' => 'July',
                     'last_name' => 'Lázaro',
                     'id_role' => 1
                ]
                ]);
    }
}
